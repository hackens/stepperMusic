#include <math.h>

const int nNotes = 2;

int dlay[nNotes];
long nextMicroSwitch[nNotes];
int state[nNotes];
boolean noteOn[nNotes];

long nextMillisTrig;

int noteToDelay(float note) {
  float thePow = pow(2.,note/12.);
  //Serial.println(thePow);
  float freq = 880.*thePow;
  //Serial.println(freq);
  float d1 = 1000000./(freq);
  
  return (int)d1;
}

void setup(){
  for(int i=0;i<nNotes;i++){
    dlay[i] = noteToDelay(3*i);
    nextMicroSwitch[i] = 0;
    state[i] = LOW;
    noteOn[i] = true;
    pinMode(i,OUTPUT);
  }
}

void loop(){
  long curMicro = micros();
  for(int i=0;i<nNotes;i++){
    if(noteOn[i] && curMicro>nextMicroSwitch[i]){
      if(state[i]==LOW){
        state[i]=HIGH;
      } else {
        state[i]=LOW;
      }
      digitalWrite(i,state[i]);
      nextMicroSwitch[i] += dlay[i];
    } else {
      digitalWrite(i,LOW);
    }
  }
  long curMillis = millis();
  if(curMillis>nextMillisTrig){
    noteTrig();
  }
}

int partition1[] = {-6,-18,-18,-15,-18,-6,-11,-15,-20,-25,-20,-18,-15,-11,
                   -6,-18,-18,-15,-18,-6,-11,-15,-20,-25,-20,-18,-15,-11,
                   -18,-18,-15,-18,-6,-11,-15,-20,-25,-20,-18,-15,-11,
                   -18,-18,-15,-18,-6,-11,-15,-20,-25,-20,-18,-15,-11};
int noteLen1[] = {1,1,1,3,1,1,1,1,1,1,1,1,1,1,
                 1,1,1,3,1,1,1,1,1,1,1,1,1,1,
                 1,2,3,1,1,1,1,1,1,1,1,1,1,
                 1,2,3,1,1,1,1,1,1,1,1,1,1};
int noteMult = 1000/8;


int partition2a[] = {
  12,7,12,7,5,7,5,3,0,-2,0,0,0,-2,-2,-2,
  12,7,12,7,5,7,5,3,0,-2,0,0,0,-2,-2,-2,
  10,7,10,7,5,7,5,3,0,-2,0,0,0,-2,-2,-2,
  10,7,10,7,5,7,5,3,0,-2,14,14,14,15,15,15
};
int partition2b[] = {
  -14,-14,
  -7,255,-7,-7,255,-7,-7,255,-7,255,255,255,255,255,-9,-7,
  -14,255,-14,-14,255,-14,-14,255,-14,255,255,255,255,255,-17,-14,
  -12,255,-12,-12,255,-12,-12,255,-12,255,255,255,255,255,-17,-14,
  -12,255,-12,-12,255,-12,-12,255,-9,-9,-9,-9,-14,-14
};

int partition3a[] = {
  255,255,255,255,1,3,6,3,255,255,255,255,3,1,-1,-4,
  -4,-4,-1,1,1,6,6,3,3,255,3,3,8,6,3,3,
};
int partition3b[] = {
  -1,-1,-1,-1,255,255,255,255,-6,-6,-6,-6,255,255,255,255,
  -4,-4,-4,-4,255,255,255,255,-8,-8,-8,-8,-9,-9,-9,-9,
};


int curNote = 0;

void noteTrig1(){
  if(noteLen1[curNote]>0){
    noteOn[0] = true;
    dlay[0] = noteToDelay(partition1[curNote]);
    nextMillisTrig += noteMult*noteLen1[curNote];
  } else {
    noteOn[0] = false;
    nextMillisTrig -= noteMult*noteLen1[curNote];
  }
  curNote = (curNote+1)%(sizeof(partition1)/sizeof(partition1[0]));
}

void noteTrig2(){
  if(partition2a[curNote]<255){
    noteOn[0] = true;
    dlay[0] = noteToDelay(partition2a[curNote]);
  } else {
    noteOn[0] = false;
  }
  if(partition2b[curNote]<255){
    noteOn[1] = true;
    dlay[1] = noteToDelay(partition2b[curNote]);
  } else {
    noteOn[1] = false;
  }
  nextMillisTrig += noteMult;
  curNote = (curNote+1)%(sizeof(partition2a)/sizeof(partition2a[0]));
}

void noteTrig(){
  if(partition3a[curNote]<255){
    noteOn[0] = true;
    dlay[0] = noteToDelay(partition3a[curNote]);
  } else {
    noteOn[0] = false;
  }
  if(partition3b[curNote]<255){
    noteOn[1] = true;
    dlay[1] = noteToDelay(partition3b[curNote]);
  } else {
    noteOn[1] = false;
  }
  nextMillisTrig += noteMult;
  curNote = (curNote+1)%(sizeof(partition3a)/sizeof(partition3a[0]));
}
