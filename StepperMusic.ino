#include <math.h>

int dlay = 1000;

int noteToDelay(float note) {
  float thePow = pow(2.,note/12.);
  //Serial.println(thePow);
  float freq = 880.*thePow;
  //Serial.println(freq);
  float d1 = 1000000./(freq);
  return (int)d1;
}

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(9600);
  // initialize digital pin 13 as an output.
  pinMode(13, OUTPUT);
  dlay = noteToDelay(0);
  
  //Serial.println(42.);
  //Serial.println(dlay);
}



// the loop function runs over and over again forever
void loop() {
  playNote(1000000l,0);
  delay(2000);
  playNote(500000l,3);
  playNote(500000l,-5);
}

void playNote(long micro,float note){
  long dlay = noteToDelay(note);
  long dlay2 = 2*dlay;
  Serial.println(dlay2);
  while(micro>0){
    digitalWrite(13,HIGH);
    delayMicroseconds(dlay);
    digitalWrite(13,LOW);
    delayMicroseconds(dlay);
    micro -= dlay2;
  }
}

