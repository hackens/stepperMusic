#include <math.h>

const int nNotes = 2;
const int pin0 = 2;
const int pinDisable = 8;

int dlay[nNotes];
long nextMicroSwitch[nNotes];
int state[nNotes];
boolean noteOn[nNotes];

int millisDlay = 0;
long nextMillisTrig;

const int partitionLength = 1<<11;
byte partition[partitionLength];
int partitionPos = 0;
int tempo = 0;
int ticksPerBeat = 0;
int nInstr = 0;
int actualLength = 0;


int noteToDelay(float note) {
  float thePow = pow(2.,note/12.);
  //Serial.println(thePow);
  float freq = 880.*thePow;
  //Serial.println(freq);
  float d1 = 1000000./(freq);
  
  return (int)d1;
}

void setup(){
  partitionSetup();
  Serial.println("Partition recue. Debut du playback...");
  
  for(int i=0;i<nInstr;i++){
    dlay[i] = noteToDelay(0);
    nextMicroSwitch[i] = micros();
    state[i] = LOW;
    noteOn[i] = false;
    pinMode(pin0+i,OUTPUT);
    pinMode(pinDisable+i,OUTPUT);
  }
  nextMillisTrig = millis();
}

void loop(){
  long curMicro = micros();
  for(int i=0;i<nInstr;i++){
    if(noteOn[i] && curMicro>nextMicroSwitch[i]){
      if(state[i]==LOW){
        state[i]=HIGH;
      } else {
        state[i]=LOW;
      }
      digitalWrite(pin0+i,state[i]);
      nextMicroSwitch[i] += dlay[i];
    } else {
      digitalWrite(pin0+i,LOW);
    }
  }
  long curMillis = millis();
  if(curMillis>nextMillisTrig){
    noteTrig();
  }
}


int curNote = 0;

void noteTrig(){
  //Serial.print("Notetrig : ");
  for(int i=0;i<nInstr;i++){
    int v = partition[partitionPos]&0xff;
    if(v==32){
      noteOn[i] = false;
      digitalWrite(pinDisable+i,HIGH);
    } else {
      noteOn[i] = true;
      digitalWrite(pinDisable+i,LOW);
      int pitch = ((int)v)-64;
      //Serial.println(pitch);
      dlay[i] = noteToDelay(pitch);
    }
    partitionPos = (partitionPos+1)%actualLength;
  }

  for(int i=0;i<nInstr;i++)
    nextMicroSwitch[i] = micros();
  nextMillisTrig = millis() + millisDlay;
}
