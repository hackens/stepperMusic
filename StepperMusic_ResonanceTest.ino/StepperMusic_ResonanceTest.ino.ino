const int nNotes = 2;
const int pin0 = 2;
const int pinDisable = 9;

int nextMicroSwitch;
int state = LOW;
long dlay = 1000000;
String strBuf;

void setup() {
  pinMode(pin0, OUTPUT);
  nextMicroSwitch = micros();
}

void loop() {
  long curMicro = micros();
  if(curMicro>nextMicroSwitch){
    if(state==LOW){
      state=HIGH;
    } else {
      state=LOW;
    }
    digitalWrite(pin0,state);
  }
  if(Serial.available()){
    char rec = Serial.read();
    if(rec == '\n'){
      float freq = strBuf.toFloat() + .1;
      dlay = (long)(500000./freq);
      strBuf = "";
    } else {
      strBuf += rec;
    }
  }
  nextMicroSwitch += dlay;
}

