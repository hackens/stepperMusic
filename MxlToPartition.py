# -*- coding: utf-8 -*-



import os
import xml.etree.ElementTree as ET

nInstr = 2

def getPitchFrom(pitchNode):
    step = pitchNode.find("step").text
    octave = int(pitchNode.find("octave").text)
    alter = pitchNode.find("alter")
    if alter != None:
        alter = int(alter.text)
    return pitchOfSOA(step,octave,alter)

def pitchOfSOA(step,octave,alter):
    pitch = 0
    stepDict = {"A":0 , "B":2, "C":-9, "D":-7, "E":-5, "F":-4,"G":-2}
    if step not in stepDict:
        print("Cannot get step from pitchNode")
        return 0
    else:
        pitch += stepDict[step]
    pitch += 12*(octave-4)
    if alter != None:
        pitch += alter
    return pitch    

def pitchChar(pitch):
    while pitch<-30:
        pitch += 12
    while pitch>60:
        pitch -= 12
    return chr(pitch+64)


print("Convertit un fichier MusicXML partwise en partition pour StepperMusic")
print("/!\ Utiliser seulement des parties monophoniques. Ne gère pas la polyphonie.")

print("Path to a MusicXML file : ",end="")
path = input()
out = ""

filename, file_extension = os.path.splitext(path)
outPath = filename + ".stm"

#
# La partition est de la forme :
# . tempo
# . nombre de ticks par temps+31
# . nombre d'instruments+31
# . Pour chaque instrument
#    . Pour chaque tick
#       . note+64, ou 'Space' si silence
#       . durée+31
#    . `!`
#


tree = ET.parse(path)
root = tree.getroot()
# print(root.tag, root.attrib)

part_list = root.find("part-list")
# print(part_list.tag, part_list.attrib)
parts = []
for score_part in part_list.iter("score-part"):
    parts.append((score_part.attrib['id'],score_part[0].text))



print("Le fichier contient ces parties :")
for p in parts:
    print(p)
print("Sélectionner jusqu'à",nInstr," parties, qui seront jouées par les steppers :")
selected_parts = []
while len(selected_parts)<nInstr:
    print("(entrer l'id correspondant, ou Entrée pour arrêter)", \
    len(selected_parts),":",end=" ")
    id = input()
    if id=="":
        break
    found = False
    for p in parts:
        if p[0] == id:
            selected_parts.append(p[0])
            found = True
            break
    if not found:
        print("Cette partie n'existe pas. Réessayez.")

print(selected_parts)

shownTypeAndDuration = False


partOut = [""]*len(selected_parts)
for part in root.iter("part"):
    if part.attrib["id"] not in selected_parts:
        continue
    i = selected_parts.index(part.attrib["id"])
    for measure in part.iter("measure"):
        for note in measure.iter("note"):
            pitchNode = note.find("pitch")
            if pitchNode == None:
                partOut[i] += " "
            else:
                pitch = getPitchFrom(pitchNode)
                partOut[i] += pitchChar(pitch)
            duration = int(note.find("duration").text)
            partOut[i] += chr(duration+63)
            if not shownTypeAndDuration:
                shownTypeAndDuration = True
                print("A ",note.find("type").text," note corresponds to ",duration," ticks.")
    partOut[i] += "!"



print("Choisir le tempo : ",end="")
tempo = int(input())
print("Choisir le nombre de ticks par temps : ", end="")
tickPerBeat = int(input())
while tempo>127:
	tempo = tempo//2
	tickPerBeat *= 2
out += chr(tempo)
out += chr(tickPerBeat+31)
    
out += chr(len(selected_parts)+31)
    
for i in range(len(selected_parts)):
    out += partOut[i]

    
print("Ecrire dans le fichier ",outPath," ? (y/n)")
if input().lower().startswith("y"):
    with open(outPath,"w") as f:
        f.write(out)
    print("Fait.")
else:
    print(out)
