# StepperMusic

## Description générale
Ce projet vise à faire jouer de la musique à des moteurs pas-à-pas 
(_stepper motors_). Le dispositif est rangé dans une boîte, à l'avant de
laquelle on voit les moteurs. À l'arrière se trouvent des branchements pour
connecter StepperMusic à un ordinateur, et pour l'alimenter. On y trouve aussi
un bouton permettant de faire redémarrer le dispositif.

## Utilisation

### Facile
Pour une première utilisation, il suffit de trouver une alimentation 12V
(_douze volts, pas neuf et encore moins seize_) et de brancher la boîte par la
prise située à l'arrière. Lors du branchement, StepperMusic devrait émettre un
léger son aigu pendant environ une seconde, puis l'arrêter. Dix secondes plus
tard, une partition par défaut devrait démarrer. Enjoy ! Lorsque vous en aurez 
assez de saigner des oreilles, vous pouvez appuyer sur le bouton à l'arrière de 
la boîte, qui vous donnera dix secondes de répit, juste assez pour débrancher le
tout.

### Avancé
StepperMusic sait aussi jouer des partitions que l'on lui envoie depuis un
ordinateur. Pour cela, il faut le connecter à l'aide d'un câble USB. On peut
alors communiquer par transmission série (9600 baud) avec l'Arduino qui contrôle
les moteurs. Il suffit d'envoyer sur le port série correspondant une partition 
(dansle format spécifique - voir plus bas) avant le démarrage de la partition 
pardéfaut. Lorsque la transmission a été effectuée, StepperMusic devrait 
commencer à jouer en boucle la partition fournie.

Pour pouvoir lire de façon pratique l'interaction série entre l'ordinateur et
l'Arduino, il est possible d'utiliser le moniteur série de l'Arduino.

#### Où trouver des partitions ?
Les partitions que l'on envoie à StepperMusic doivent être dans un format
spécifique. Le dossier `partitions_v3/` contient un certain nombre de
partitions. Pour créer de nouvelles partitions on peut utiliser
le script `MxlToPartition.py`. Celui-ci convertit un fichier MusicXML non
compressé (`.xml`) en partition StepperMusic (`.stm`). Envoyer une partition à
StepperMusic revient à lui envoyer le contenu du fichier `.stm`.


## Montage
Le dispositif est composé d'une Arduino et de deux steppers, avec chacun un 
Pololu de contrôle. Le nombre de steppers pourrait être augmenté mais il est 
limité par la vitesse de calcul de l'Arduino, et par le nombre de pins 
disponibles.
Les pins 2 et 3 contrôlent les steps, et les pins 8 et 9 contrôlent la 
désactivation des steppers (lorsqu'un stepper est maintenu sans être désactivé 
il fait un son aigu horrible).
L'alimentation 12V sert à la fois aux moteurs et à l'Arduino.
