

void partitionSetup(){
  Serial.begin(9600);
  Serial.println("En attente de la partition...");
  while(receivePartition());
  millisDlay = 60000/(tempo*ticksPerBeat);
}

/**
 * Reads one byte, writes it into the corresponding field, and returns
 */
bool receivePartition(){
  while(!Serial.available());
  int v = Serial.read()&0xff;
  //Serial.println(v);
  if (tempo == 0) {
    tempo = v;
    Serial.println(tempo);
    return true;
  } else if (ticksPerBeat == 0) {
    ticksPerBeat = v-31;
    Serial.println(ticksPerBeat);
    return true;
  } else if (nInstr == 0) {
    nInstr = v-31;
    Serial.println(nInstr);
    if(nInstr>nNotes){
      Serial.print("Trop d'instruments. Max=");
      Serial.println(nNotes);
      return false;
    }
    return true;
  } else if (v==33) { // Fin de l'instrument
    partitionPos[curInstr] = 0;
    curInstr++;
    return curInstr<nInstr;
  }
  partition[curInstr][partitionPos[curInstr]] = (byte)v;
  partitionPos[curInstr]++;
  if(partitionPos[curInstr]>partitionLength){
    Serial.println("Partition trop longue");
  }
  //Serial.println("Received one byte!");
  return true;
}

/**
 * La partition est de la forme :
 * . tempo
 * . nombre de ticks par temps+31
 * . nombre d'instruments+31
 * . Pour chaque instrument
 *    . Pour chaque tick
 *       . note+64, ou 'Space' si silence
 *       . durée+63
 *    . `!`
 */
