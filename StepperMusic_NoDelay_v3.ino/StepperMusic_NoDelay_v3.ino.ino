#include <math.h>

const int nNotes = 2;
const int pin0 = 2;
const int pinDisable = 8;

int dlay[nNotes];
long nextMicroSwitch[nNotes];
int state[nNotes];
boolean noteOn[nNotes];

int millisDlay = 0;
long nextMillisTrig;

const int partitionLength = 1<<11;
byte partition[nNotes][partitionLength];
int partitionPos[nNotes];
int tempo = 0;
int ticksPerBeat = 0;
int nInstr = 0;
int curInstr = 0;
int curPartitionPos = 0;
int nextPartitionTrig[nNotes];


int noteToDelay(float note) {
  float thePow = pow(2.,note/12.);
  //Serial.println(thePow);
  float freq = 880.*thePow;
  //Serial.println(freq);
  float d1 = 1000000./(freq);
  
  return (int)d1;
}

void setup(){
  for(int i=0;i<nNotes;i++){
    dlay[i] = noteToDelay(0);
    nextMicroSwitch[i] = micros();
    state[i] = LOW;
    noteOn[i] = false;
    pinMode(pin0+i,OUTPUT);
    pinMode(pinDisable+i,OUTPUT);
    digitalWrite(pinDisable+i,HIGH);
  }
  partitionSetup();
  Serial.println("Partition recue. Debut du playback...");
  nextMillisTrig = millis();
}

void loop(){
  long curMicro = micros();
  for(int i=0;i<nInstr;i++){
    if(noteOn[i] && curMicro>nextMicroSwitch[i]){
      if(state[i]==LOW){
        state[i]=HIGH;
      } else {
        state[i]=LOW;
      }
      digitalWrite(pin0+i,state[i]);
      nextMicroSwitch[i] += dlay[i];
    } else {
      digitalWrite(pin0+i,LOW);
    }
  }
  long curMillis = millis();
  if(curMillis>nextMillisTrig){
    noteTrig();
  }
}


int curNote = 0;

void noteTrig(){
  //Serial.print("Notetrig : ");
  for(int i=0;i<nInstr;i++){
    if(curPartitionPos>=nextPartitionTrig[i]){
      if (partitionPos[i]>=partitionLength || partition[i][partitionPos[i]]==0){
        partitionEnd();
      }
      //Serial.print("Triggering note [");Serial.print(i);Serial.print("]:");
      int v = partition[i][partitionPos[i]];
      if(v==32){
        noteOn[i] = false;
        digitalWrite(pinDisable+i,HIGH);
        //Serial.print("__,");
      } else {
        noteOn[i] = true;
        digitalWrite(pinDisable+i,LOW);
        int pitch = ((int)v)-64;
        //Serial.println(pitch);
        dlay[i] = noteToDelay(pitch);
        //Serial.print(pitch); Serial.print(",");
      }
      int duration = ((int)partition[i][partitionPos[i]+1])-63;
      nextPartitionTrig[i] += duration;
      partitionPos[i] += 2;
      //Serial.print(duration);Serial.println();
    }
  }
  curPartitionPos++;
  
  for(int i=0;i<nInstr;i++)
    nextMicroSwitch[i] = micros();
  nextMillisTrig = millis() + millisDlay;
}

void partitionEnd(){
  curPartitionPos=0;
  for(int i=0;i<nInstr;i++){
    partitionPos[i]=0;
    nextPartitionTrig[i]=0;
  }
}

