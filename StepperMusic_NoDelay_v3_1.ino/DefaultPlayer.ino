

void dfltLoop(){
  while(millis()<nextMillisTrig){
    long curMicro = micros();
    if(curMicro>dfltMicroSwitch){
        if(dfltState==LOW){
          dfltState=HIGH;
        } else {
          dfltState=LOW;
        }
        digitalWrite(pinDisable,LOW);
        digitalWrite(pin0,dfltState);
        dfltMicroSwitch += dfltDlay;
      } else {
        digitalWrite(pin0,LOW);
      }
  }
  dfltNoteTrig();
}

void dfltNoteTrig(){
  dfltDlay = noteToDelay(dfltPartition[dfltPartitionPos]);
  dfltPartitionPos = (dfltPartitionPos+1)%dfltPartitionLength;
  nextMillisTrig = millis() + dfltMillisDlay;
  //Serial.println(nextMillisTrig);
}
